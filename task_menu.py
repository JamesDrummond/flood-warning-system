import Task1A, Task1B, Task1C, Task1D, Task1E, Task1F, Task2A, Task2B, Task2C, Task2D, Task2E, Task2F, Task2G, floodsystem.weathersystem, mailing_list

tasks = {'1A': 'Task1A.run()',
         '1B': 'Task1B.run()',
         '1C': 'Task1C.run()',
         '1D': 'Task1D.run()',
         '1E': 'Task1E.run()',
         '1F': 'Task1F.run()',
         '2A': 'Task2A.run()',
         '2B': 'Task2B.run()',
         '2C': 'Task2C.run()',
         '2D': 'Task2D.run()',
         '2E': 'Task2E.run()',
         '2F': 'Task2F.run()',
         '2G': 'Task2G.run()',
         'WT': 'print(floodsystem.weathersystem.weather_table(var))',
         'RW': 'print(floodsystem.weathersystem.raw_weather(var))',
         'ML': 'print(mailing_list.retrieve(var))',
         'SE': 'print(floodsystem.weathersystem.show_errors(var))',
         'FR': 'print(floodsystem.weathersystem.flood_risk(var))'}

def menu(task, var):
    eval(tasks[task])
