from tkinter import Tk, Label, Button, Entry, Frame
from visual_output import run
from mailing_list import add, remove
import scheduler

class Page(Frame):
    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
    def show(self):
        self.lift()

class Page1(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       
       name_text = Label(self, text="Name:")
       name_entry = Entry(self)
       email_text = Label(self, text="Email:")
       email_entry = Entry(self)
       postcode_text = Label(self, text="Postcode:")
       postcode_entry = Entry(self)
       
       add_user = Button(self, text="Add Recipient", command= lambda: add(name_entry.get(), email_entry.get(), postcode_entry.get()))
       
       name_text.pack()
       name_entry.pack()
       email_text.pack()
       email_entry.pack()
       postcode_text.pack()
       postcode_entry.pack()
       add_user.pack()
       
class Page2(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       
       search_text = Label(self, text="Search:")
       search_entry = Entry(self)
       
       find_user = Button(self, text="Find Recipient", command= lambda: run('ML', search_entry.get()))
       
       search_text.pack()
       search_entry.pack()
       find_user.pack()
       
class Page3(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       
       search_text = Label(self, text="Search:")
       search_entry = Entry(self)
       
       remove_user = Button(self, text="Remove Recipient", command= lambda: remove(search_entry.get()))
       
       search_text.pack()
       search_entry.pack()
       remove_user.pack()
       
class Page4(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       
       freq_text = Label(self, text="Frequency(seconds):")
       freq_entry = Entry(self)
       rad_text = Label(self, text="Radius(km):")
       rad_entry = Entry(self)
       level_text = Label(self, text="Warning Level(0-10):")
       level_entry = Entry(self)
       refresh_text = Label(self, text="Refresh each time (y/n):")
       refresh_entry = Entry(self)
       
       freq_text.pack()
       freq_entry.pack()
       rad_text.pack()
       rad_entry.pack()
       level_text.pack()
       level_entry.pack()
       refresh_text.pack()
       refresh_entry.pack()
       
       buttonframe = Frame(self)
       buttonframe.pack(side="bottom", fill="x", expand=False)
       Button(buttonframe, text='Start', command= lambda: scheduler.start(freq_entry.get(), rad_entry.get(), level_entry.get(), refresh_entry.get())).pack(side="right")
       
class MainView(Frame):
    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
        p1 = Page1(self)
        p2 = Page2(self)
        p3 = Page3(self)
        p4 = Page4(self)

        buttonframe = Frame(self)
        container = Frame(self)
        buttonframe.pack(side="top", fill="x", expand=False)
        container.pack(side="top", fill="both", expand=True)
        
        p1.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
        p2.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
        p3.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
        p4.place(in_=container, x=0, y=0, relwidth=1, relheight=1)

        Button(buttonframe, text="Add to Mailing List", command=p1.lift).pack(side="left")
        Button(buttonframe, text="Search Mailing List", command=p2.lift).pack(side="left")
        Button(buttonframe, text="Remove from Mailing List", command=p3.lift).pack(side="left")
        Button(buttonframe, text="Start Scheduler", command=p4.lift).pack(side="left")

        p1.show()

def admin():
    root = Tk()
    main = MainView(root)
    main.pack(side="top", fill="both", expand=True)
    root.wm_geometry("500x400")
    root.wm_title("Admin Options")
    root.mainloop()