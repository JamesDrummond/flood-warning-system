
from apscheduler.schedulers.background import BackgroundScheduler
from send_email import send
from apscheduler.triggers.interval import IntervalTrigger


def start(x, rad, level, refresh):
    if x == '':
        x = 60
    if rad == '':
        rad = 5
    if level == '':
        level = 1
    if refresh == '':
        refresh = 'y'
    trigger = IntervalTrigger(seconds=int(x))
    scheduler = BackgroundScheduler()
    scheduler.add_job(lambda: send(rad, level, refresh), trigger, id='email')
    scheduler.start()