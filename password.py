from tkinter import messagebox, Label, Button, Tk, Entry
import admin
username = ("Admin")
password = ("123")

def run():
    def try_login():
#        print("Trying to login...")
        if username_guess.get() == username and password_guess.get() == password:
            window.destroy()
            admin.admin()
            
        else:
            messagebox.showinfo("-- ERROR --", "Please enter valid infomation!", icon="warning")
    
    window = Tk()
    window.title("Log-In")
    window.geometry("200x150")
    
    username_text = Label(window, text="Username:")
    username_guess = Entry(window)
    password_text = Label(window, text="Password:")
    password_guess = Entry(window, show="*")
    
    #attempt to login button
    attempt_login = Button(window, text="Login", command=try_login)
    
    username_text.pack()
    username_guess.pack()
    password_text.pack()
    password_guess.pack()
    attempt_login.pack()
    
    window.mainloop()