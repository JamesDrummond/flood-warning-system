import pytest
import floodsystem.weathersystem as ws
from floodsystem.weatherstation import WeatherStation

def test_nearest_weather_station():
    test = ws.nearest_weather_station("CB3 9DQ")
    assert test.name == 'CAMBRIDGE'

def test_raw_weather():
    test1 = ws.raw_weather('')
    test2 = ws.raw_weather("CB3 9DQ")
    
    assert test1 == "No location entered"
    assert len(test2) == 7
    assert test2[0] == 'CAMBRIDGE'
    assert len(test2[2]) == len(test2[4]) == len(test2[6])
    
def test_weather_table():
    test1 = ws.weather_table("")
    test2 = ws.weather_table("CB3 9DQ")
    assert test2
    assert test1 == "No location entered"
    assert len(test2) == 530
    
def test_flood_risk():
    test1 = ws.flood_risk('')
    test2 = ws.flood_risk('Los Angeles')
    test3 = ws.flood_risk('CB3 9DQ')
    
    assert test1 == "No location entered"
    assert test2 == "No stations in range"
    assert len(test3) == 3
    
def test_average_rain():
    test_station1 = WeatherStation("test-1-id",
                                   "station-1",
                                   (1, 1),
                                   [0, 0, 0, 0, 0],
                                   [10, 0, 10, 0, 10],
                                   [0, 0, 0, 0, 0],
                                   '2017-02-17 14:00:00+00:00',
                                   None)
    test = ws.average_rain(test_station1)
    assert test == 6

def test_weather_severity():
    test_station1 = WeatherStation("test-1-id",
                                   "station-1",
                                   (1, 1),
                                   [12, 21, 11, 29, 20],
                                   [0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0],
                                   '2017-02-17 14:00:00+00:00',
                                   None)
    test = ws.weather_severity(test_station1)
    assert test == 0.6
    
def test_show_errors():
    test1 = ws.show_errors('')
    test2 = ws.show_errors('Los Angeles')
    test3 = ws.show_errors('CB3 9DQ')
    
    assert test1
    assert test2 == "There are no errors in range"
    assert test3
    
    