# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 13:48:20 2017

@author: Sam
"""
from .utils import sorted_by_key



def stations_level_over_threshold(stations, tol):
    """Takes a list of MonitoringStation items and returns a sorted list of stations 
    ranked from highest relative level to lowest. Relative level is the percentage score 
    of the normal range that the current level stands at. Only stations above a 
    set tolerance are added to the list"""
    # Create an empty list
    stations_over_threshold = []
    # Add each station with a curent relative level over the tolerance
    for station in stations:
        if station.relative_water_level() == None:
            pass
        elif station.relative_water_level() > tol:
            stations_over_threshold.append((station, station.relative_water_level()))
    # Sort the list    
    stations_over_threshold = sorted_by_key(stations_over_threshold, 1, True)    
    return stations_over_threshold
        
def stations_highest_rel_level(stations, N):
    """Takes a list of MonitoringStation items and returns the top 'N' stations 
    with the highest relative level"""
    # Create an empty list
    stations_highest_level = []
    #Add every station to the list
    for station in stations:
        if station.relative_water_level() == None:
            pass
        else:
            stations_highest_level.append((station, station.relative_water_level()))
    # Sort the list and return the first N stations
    stations_highest_level = sorted_by_key(stations_highest_level, 1, True)
    stations_highest_level = stations_highest_level[:N]
    return stations_highest_level