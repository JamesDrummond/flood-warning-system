"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += " measure id: {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

        
    def typical_range_consistent(self):
        """ Takes the typical range and returns True if it is consistent
        or False if not, this is if there is no data or the high point
        is lower than the low point"""
        if self.typical_range == None:
            return False
        elif self.typical_range[0] > self.typical_range[1]:
            return False
        else:
            return True
            
            
    def relative_water_level(self):
        if self.latest_level == None:
            relative_level = None
        elif self.typical_range_consistent():
            zero_range = self.typical_range[1]-self.typical_range[0]
            zero_level = self.latest_level-self.typical_range[0]
            relative_level = zero_level/zero_range
        else:
            relative_level = None
        return relative_level
        
def inconsistent_typical_range_stations(stations):
    """ Takes a list of stations and returns a list of the station names - in
    alphabetical order - for the stations with inconsistent range data """
    # Create empty list
    error_range = []
    for station in stations:
        # For each station test if range data is consistent
        if station.typical_range_consistent():
            pass
        else:
            # If not consistent add name to list
            error_range.append(station.name)
    # Sort the list of names
    error_range.sort()
    return error_range
    