"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key, haversine

def rivers_with_station(stations):
    """ Takes a list of stations and returns a set of rivers with stations"""
    rivers = set()
    for station in stations:
        rivers.add(station.river)
    return rivers
    
def stations_by_river(stations):
    """ Takes a list of stations, creates a dictionary with rivers as Keys and 
    a list of the names of stations on each of the rivers as Values, then sorts
    the lists and returns the dictionary"""
    rivers_dict = {}
    for station in stations:
        river = station.river
        if river in rivers_dict:
            rivers_dict[river].append(station.name)
        else:
            rivers_dict[river] = []
            rivers_dict[river].append(station.name)
    for river in rivers_dict:
        rivers_dict[river].sort()
    return rivers_dict

def stations_by_distance(stations, p):
    """This function odrers all monitoring stations in the list "stations" by
    the attribute "distance" and creates a list of tuples showing monitoring station
    and distance from a certain coordinate ("p")."""
    #create empty list
    distances=[]

    # create list of stations with their distance from p
    for station in stations:
        d = haversine(p, station.coord)
        distances.append((station, d))
     
    # sort the list from closest to furthest
    return sorted_by_key(distances,1)
        

def stations_within_radius(stations, centre, r):
    """This function selects all monitoring stations in the list "stations" 
    that are within a radius of "r" and prints them as a list of of monitoring stations"""
    
    #create empty list
    locus=[]

    #collect all stations inside radius
    for station in stations:
        d = haversine(centre, station.coord)
        if d > r:
            pass
        else:
            locus.append(station)
    return locus
    
def rivers_by_station_number(stations, N=5):
    """ Takes a list of stations and a number (N) - set to 5 by default in case no 
    argument is given. Creates a dictionary of river Keys and number of stations
    on each river Values. Converts this dictionary in to a list of (Key, Value)
    tuples. Sorts the list by number of stations from highest to lowest. Creates
    a list that is a subset of the list of all rivers, only containing the N
    rivers with the greatest number of stations - in the case that multiple rivers
    have the same number of stations as the Nth river these are also included.
    Returns this subset """
    rivers_dict = {}
    for station in stations:
        river = station.river
        if river in rivers_dict:
            rivers_dict[river] += 1
        else:
            rivers_dict[river] = 1
    number_of_stations = []
    for river, number in rivers_dict.items():
        number_of_stations.append((river, number))
    sorted_stations = sorted_by_key(number_of_stations, 1, True)
    min_stations = sorted_stations[N-1][1]
    max_stations = []
    for station in sorted_stations:
        if station[1] >= min_stations:
            max_stations.append(station)
    return max_stations