from dateutil import parser

weather_codes = {0: "Clear night", 
                 1: "Sunny day",
                 2: "Partly cloudy",
                 3: "Partly cloudy",
                 4: "Cloudy with a chance of meatballs", 
                 5: "Mist",
                 6: "Fog",
                 7: "Cloudy",
                 8: "Overcast",
                 9: "Light rain shower",
                 10:"Light rain shower",
                 11:"Drizzle",
                 12:"Light rain",
                 13:"Heavy rain shower",
                 14:"Heavy rain shower",
                 15:"Heavy rain",
                 16:"Sleet shower",
                 17:"Sleet shower",
                 18:"Sleet",
                 19:"Hail shower",
                 20:"Hail shower",
                 21:"Hail",
                 22:"Light snow shower",
                 23:"Light snow shower",
                 24:"Light snow",
                 25:"Heavy snow shower",
                 26:"Heavy snow shower",
                 27:"Heavy snow",
                 28:"Thunder shower",
                 29:"Thunder shower",
                 30:"Thunder"}

class WeatherStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, name, coord, weather, rain, temp, time, weather_types = None):

        self.station_id = station_id
        self.name = name
        self.coord = coord
        self.weather = weather
        self.rain = rain
        self.temp = temp
        self.time = parser.parse(time)
        
        weather_types = []
        for n in self.weather:
            weather_types.append(weather_codes[n])
            
        self.weather_types = weather_types
    


    def __repr__(self):
        d =  "Station name:      {}\n".format(self.name)
        d += "   id:             {}\n".format(self.station_id)
        d += "   coordinates:    {}\n".format(self.coord)
        d += "   weather:        {}\n".format(self.weather_types)
        d += "   chance of rain: {}\n".format(self.rain)
        d += "   temperature:    {}\n".format(self.temp)
        d += "   time of reading:{}\n".format(self.time)
        return d