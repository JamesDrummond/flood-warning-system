import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def polyfit(dates, levels, p):
    """Create a best fit polynomial line of degree 'p' to fit historic water level data"""
    # Convert the datetime data to numbers
    x=matplotlib.dates.date2num(dates)
    # Apply a polynomial that treats the oldest recorded time data as zero time
    p_coeff = np.polyfit(x -x[0], levels, p)
    poly = np.poly1d(p_coeff)
    print(poly)
    return (poly, x[0])
    
def grad(dates, levels, p):
    """Find the gradient of the historic water level data and return the gradient at the current time"""
    # Convert the datetime data to numbers
    x=matplotlib.dates.date2num(dates)
    # Apply a polynomial that treats the oledst recorded time data as zero time
    p_coeff = np.polyfit(x -x[0], levels, p)
    poly = np.poly1d(p_coeff)
    # Differentiate the polynomial
    grad = np.polyder(poly)
    return grad(0)
    
def cleanlist(stations):
    """ Given a list of MonitoringStations return a list of stations whith consistent
    water level data, also return a list of stations with no current water level 
    data or inconsistent data"""
    # Create the empty lists
    thelist =[]
    thebadlist =[]
    for station in stations:
        # Sort the stations
        if station.relative_water_level() != None and station.typical_range_consistent() and station.relative_water_level() <10:
            thelist.append(station)
        else:
            thebadlist.append(station.name)
    return thelist, thebadlist