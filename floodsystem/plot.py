
import matplotlib.pyplot as plt
#from datetime import datetime, timedelta
from floodsystem.analysis import polyfit
import matplotlib


def plot_water_levels(station, dates, levels):
    """ For a given MonitoringStation and historic water level data plot a graph 
    that shows the water level over time and the normal minimum and maximum levels"""
    fig = plt.figure(station.name)
    # Plot
    fig = plt.plot(dates, levels)
    fig = plt.hlines(station.typical_range[0],dates[0],dates[-1])
    fig = plt.hlines(station.typical_range[1],dates[0],dates[-1])
    # Add axis labels, rotate date labels and add plot title
    fig = plt.xlabel('date')
    fig = plt.ylabel('water level (m)')
    fig = plt.xticks(rotation=45);
    fig = plt.title(station.name)
    
    # Display plot
    fig = plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    print(station.name, "Current Water Level:", station.latest_level, "Typical Range:" , station.typical_range[0]," - ", station.typical_range[1])

    return fig    
    

def plot_water_levels_with_fit(station, dates, levels, p):
    """ For a given MonitoringStation and historic water level data plot a graph 
    that shows the water level over time, the normal minimum and maximum levels 
    and polynomial fit line of degree 'p' """
    # Create best fit line
    poly, d0 = polyfit(dates, levels, p)
    plt.figure(station.name)
    # Plot
    plt.plot(dates, levels)
    plt.plot(dates, poly(matplotlib.dates.date2num(dates)-d0), 'r')
    plt.hlines(station.typical_range[0],dates[0],dates[-1])
    plt.hlines(station.typical_range[1],dates[0],dates[-1])
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
    
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    
