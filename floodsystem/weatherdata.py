"""This module provides interface for extracting statiob data from
JSON objects fetched from the Internet and

"""

from .weatherstation import WeatherStation
from . import datafetcher_weather

def build_weather_list(use_cache=True):
    """Build and return a list of all river level monitoring stations
    based on data fetched from the Environment agency. Each station is
    represented as a MonitoringStation object.

    The available data for some station is incomplete or not
    available.

    """

    # Fetch weather data
    data = datafetcher_weather.fetch_weather_data(use_cache)
    # Build list of WeatherStation objects
    weather_stations = []

    for i in range(len(data['SiteRep']['DV']['Location'])):
        station_id = data['SiteRep']['DV']['Location'][i]['i']
        name = data['SiteRep']['DV']['Location'][i]['name']
        coord = (float(data['SiteRep']['DV']['Location'][i]['lat']), float(data['SiteRep']['DV']['Location'][i]['lon']))
        weather_type = []
        for n in range(len(data['SiteRep']['DV']['Location'][i]['Period'])):
            for m in range(len(data['SiteRep']['DV']['Location'][i]['Period'][n]['Rep'])):
                weather_type.append(int(data['SiteRep']['DV']['Location'][i]['Period'][n]['Rep'][m]['W']))
        chance_rain = []
        for z in range(len(data['SiteRep']['DV']['Location'][i]['Period'])):
            for x in range(len(data['SiteRep']['DV']['Location'][i]['Period'][z]['Rep'])):
                chance_rain.append(int(data['SiteRep']['DV']['Location'][i]['Period'][z]['Rep'][x]['Pp']))
        temperature = []
        for p in range(len(data['SiteRep']['DV']['Location'][i]['Period'])):
            for q in range(len(data['SiteRep']['DV']['Location'][i]['Period'][p]['Rep'])):
                temperature.append(int(data['SiteRep']['DV']['Location'][i]['Period'][p]['Rep'][q]['T']))
        time = data['SiteRep']['DV']['dataDate']

        try:

            s = WeatherStation(station_id=station_id,
                                  name=name,
                                  coord=coord,
                                  weather=weather_type,
                                  rain=chance_rain,
                                  time = time,
                                  temp = temperature)
            weather_stations.append(s)
        except:

            pass

    return weather_stations