from .stationdata import build_station_list, update_water_levels
from .flood import stations_level_over_threshold
from .datafetcher import fetch_measure_levels
from .analysis import grad, cleanlist
from .weatherdata import build_weather_list
from .geo import stations_by_distance, stations_within_radius
import geocoder
from datetime import timedelta
import gmplot
import webbrowser
import os

import numpy as np

def nearest_weather_station(location):
    g = geocoder.google(location)
    coords = g.latlng
    stations = build_weather_list()
    if str(g) == '<[ZERO_RESULTS] Google - Geocode>':
        return "Error with input\nPlease enter a valid location"
    else:
        station = stations_by_distance(stations,coords)[0]
        if station[1] > 25:
            return "Closest weather station is over 25km, so no valid data for location"
        else:
            return station[0]
    
    
def raw_weather(loc):
    if loc == '':
        return "No location entered"
    else:
        station = nearest_weather_station(loc)
        return station.name , 'Weather:' , station.weather_types , 'Chance of precipitation:' , station.rain , 'Temperature:' , station.temp

def weather_table(loc):
    if loc == '':
        return "No location entered"
    else:
        station = nearest_weather_station(loc)
        times = []
        for i in range(5):
            time = station.time + timedelta(hours=(3*i))
            times.append(time)
        header = "{:<8}| ".format("Time:")
        for f in times:
            header += "{:<18}".format('{:%H:%M}'.format(f))
    
        spacer = "-"*len(header)
        
        row1 = "{:<8}| ".format("Weather")
        for i in range(5):
            row1 += "{:<18}".format(station.weather_types[i])
        
        row2 = "{:<8}| ".format("Temp")
        for i in range(5):
            row2 += "{:<18}".format(station.temp[i])
        
        row3 = "{:<8}| ".format("Rain %")
        for i in range(5):
            row3 += "{:<18}".format(station.rain[i])
    
        return header + '\n' + spacer + '\n' + row1 + '\n' + row2 + '\n' + row3 + '\n' + str(station.time)
    
def heatmap(loc, zoom):
    if loc == '':
        loc = "53.78, -1.62"
        zoom = 6
    g = geocoder.google(loc)
    coords = g.latlng
    if str(g) == '<[ZERO_RESULTS] Google - Geocode>':
        pass
    else:
        gmap = gmplot.GoogleMapPlotter(coords[0],coords[1], zoom)
        stations = build_station_list()
        update_water_levels(stations)
        high_stations = stations_level_over_threshold(stations, 0.8)
        lats = []
        lngs = []
        for station in high_stations:
            lats.append(station[0].coord[0])
            lngs.append(station[0].coord[1])
        gmap.heatmap(lats, lngs, opacity = 4)
        map_name = "flood_risks.html"
        gmap.draw(map_name)
        
        webbrowser.open('file://' + os.path.realpath(map_name))



def risk_levels(all_stations):
    update_water_levels(all_stations)
    stations, errors = cleanlist(all_stations)
    weather_stations = build_weather_list()
    dt=2
    flood_risks = []
    counter = 1
    for station in stations:
        try:
            dates, levels = fetch_measure_levels(station.measure_id,
                                                 dt=timedelta(days=dt))
            # determine current increase/decrease of relative water level
            gradient = (np.arctan(grad(dates,levels,4))*2)/np.pi
            # remove any negative component from gradient and multiply by current level to determine water level risk
            water_risk = (1+gradient) * station.relative_water_level()
            
            weather_station = stations_by_distance(weather_stations,station.coord)[0][0]
            
            rain = average_rain(weather_station)
            severity = weather_severity(weather_station)
            # determine weather severity according to risk analysis
            weather_risk = rain*severity/100
            # determine overall flood risk
            score = water_risk + (weather_risk*3)
            if score > 4:
                risk = "Severe"
            elif score > 2.5:
                risk = "High"
            elif score > 1:
                risk = "Moderate"
            else:
                risk = "Low"
            flood_risks.append((station.name,station.river,score,risk))
        except:
            errors.append(station.name) 
        counter += 1
            
    return flood_risks

def flood_risk(loc):
    if loc == '':
        return "No location entered"
    else:
        g = geocoder.google(loc)
        location = g.latlng
        stations = build_station_list()
        if str(g) == '<[ZERO_RESULTS] Google - Geocode>':
            return "Error with input\nPlease enter a valid location"
        else:
            close_stations = stations_within_radius(stations, location, 5)
            if len(close_stations) != 0:
                risks = risk_levels(close_stations)
            else:
                risks = []
            if len(risks) == 0:
                risks = "No stations in range"
            return risks
    
def average_rain(station):
    total = 0
    for i in station.rain:
        total += i
    return total/len(station.rain)
    
def weather_severity(station):
    total = 0
    for i in station.weather:
        if i < 9:
            total += 0
        elif i < 13:
            total += 0.4
        elif i < 16:
            total += 0.8
        elif i < 22:
            total += 0.6
        elif i < 28:
            total += 0.2
        else:
            total += 1
    return total/len(station.weather)

def at_risk(people, rad, level):
    at_risk = set()
    stations = build_station_list()
    for person in people:
        close_stations = stations_within_radius(stations, person.coord, float(rad))
        if len(close_stations) != 0:
            risks = risk_levels(close_stations)
            for item in risks:
                if item[2] > float(level):
                    at_risk.add(person)
    at_risk = list(at_risk)
    return at_risk
    
def show_errors(loc):
    all_stations = build_station_list()
    update_water_levels(all_stations)
    if loc == '':
        stations, errors = cleanlist(all_stations)
        return errors
    else:
        g = geocoder.google(loc)
        location = g.latlng
        if str(g) == '<[ZERO_RESULTS] Google - Geocode>':
            return "Error with input\nPlease enter a valid location"
        else:
            close_stations = stations_within_radius(all_stations, location, 10)
            stations, errors = cleanlist(close_stations)
            if len(errors) == 0:
                errors = "There are no errors in range"
            return errors
    
