from visual_output import run
from tkinter import Tk, Label, Button, Entry, Frame, messagebox
from floodsystem.weathersystem import heatmap 
import password, floodsystem
import os, requests

class Page(Frame):
    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
    def show(self):
        self.lift()

class Page1(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       label = Label(self, text=
       """Your team has been tasked with building the computational backend (library) to a new real-time flood warning system for England. \n 
          The library should: \n
          Fetch real-time river level data over the Internet from the Department for Environment Food and Rural Affairs data service. \n
          Support specific data queries on river level monitoring stations. \n
          Analyse monitoring station data in order to assess the flood risk, and issue flood warnings for areas of the country.""", wraplength = 400)
       label.pack(side="top", fill="both", expand=True)
        
       

class Page2(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       label = Label(self, text=
       """Processing of monitoring station properties. \n
       Deadline:	Mid-term sign-up session \n
       Points:	4""", wraplength = 400)
       label.pack(side="top", fill="both", expand=True)
       
       buttonframe = Frame(self)
       buttonframe.pack(side="bottom", fill="x", expand=False)
       
       Button(buttonframe, text='Task1A', command= lambda: run('1A')).pack(side="left")
       Button(buttonframe, text='Task1B', command= lambda: run('1B')).pack(side="left")
       Button(buttonframe, text='Task1C', command= lambda: run('1C')).pack(side="left")
       Button(buttonframe, text='Task1D', command= lambda: run('1D')).pack(side="left")
       Button(buttonframe, text='Task1E', command= lambda: run('1E')).pack(side="left")
       Button(buttonframe, text='Task1F', command= lambda: run('1F')).pack(side="left")

class Page3(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       label = Label(self, text=
       """Milestone 2 \n
       The focus of the Milestone 2 is processing monitoring station real-time data to warn of flood risks. \n
       Deadline:	End-of-term sign-up session \n
       Points:	8""", wraplength = 400)
       label.pack(side="top", fill="both", expand=True)
       
       buttonframe = Frame(self)
       buttonframe.pack(side="bottom", fill="x", expand=False)
       
       Button(buttonframe, text='Task2A', command= lambda: run('2A')).pack(side="left")
       Button(buttonframe, text='Task2B', command= lambda: run('2B')).pack(side="left")
       Button(buttonframe, text='Task2C', command= lambda: run('2C')).pack(side="left")
       Button(buttonframe, text='Task2D', command= lambda: run('2D')).pack(side="left")
       Button(buttonframe, text='Task2E', command= lambda: run('2E')).pack(side="left")
       Button(buttonframe, text='Task2F', command= lambda: run('2F')).pack(side="left")
       Button(buttonframe, text='Task2G', command= lambda: run('2G')).pack(side="left")

class Page4(Page):
   def __init__(self, *args, **kwargs):
       Page.__init__(self, *args, **kwargs)
       
       location_text = Label(self, text="Search:")
       location_entry = Entry(self)
       
       location_text.pack()
       location_entry.pack()
       
       weather_table = Button(self, text="View Weather", command= lambda: run('WT', location_entry.get()))
       weather_raw = Button(self, text="Raw Weather", command= lambda: run('RW', location_entry.get()))
       heatmap_loc = Button(self, text='Local Heatmap', command= lambda: heatmap(location_entry.get(), 12))
       flood_risk = Button(self, text='Flood Risk', command= lambda: run('FR', location_entry.get()))
       near_errors = Button(self, text='Show errors', command= lambda: run('SE', location_entry.get()))
       
       flood_risk.pack()
       weather_table.pack()
       heatmap_loc.pack()
       weather_raw.pack()
       near_errors.pack()
       
       buttonframe = Frame(self)
       buttonframe.pack(side="bottom", fill="x", expand=False)
       Button(buttonframe, text='Heatmap', command= lambda: heatmap("53.78, -1.62", 6)).pack(side="left")
       Button(buttonframe, text='Admin', command=password.run).pack(side="right")
       Button(buttonframe, text='Refresh', command=refresh).pack(side="right")
       Button(buttonframe, text='Show errors', command= lambda: run('SE', '')).pack(side="left")
       
class MainView(Frame):
    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
        p1 = Page1(self)
        p2 = Page2(self)
        p3 = Page3(self)
        p4 = Page4(self)

        buttonframe = Frame(self)
        container = Frame(self)
        buttonframe.pack(side="top", fill="x", expand=False)
        container.pack(side="top", fill="both", expand=True)
        
        p1.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
        p2.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
        p3.place(in_=container, x=0, y=0, relwidth=1, relheight=1)
        p4.place(in_=container, x=0, y=0, relwidth=1, relheight=1)

        Button(buttonframe, text="Project Overview", command=p1.lift).pack(side="left")
        Button(buttonframe, text="Milestone 1", command=p2.lift).pack(side="left")
        Button(buttonframe, text="Milestone 2", command=p3.lift).pack(side="left")
        Button(buttonframe, text="Added Features", command=p4.lift).pack(side="left")
        Button(buttonframe, text="Close", command=root.destroy).pack(side="right")

        p1.show()
        
def refresh():
    if internet_on():
        if messagebox.askyesno("Refresh", "Are you sure? \n May take a couple of minutes"):
            floodsystem.datafetcher.fetch_station_data(False)
            floodsystem.datafetcher_weather.fetch_weather_data(False)
    else:
        if messagebox.askyesno("No internet connection", "Delete Cache?"):
            try:
                os.remove(os.path.join("cache", 'weather_data.json'))
                os.remove(os.path.join("cache", 'station_data.json'))
            except:
                pass
    
def internet_on():
    try:
        requests.get("http://www.google.com")
        return True
    except requests.ConnectionError:
        return False
        
if __name__ == "__main__":
    root = Tk()
    main = MainView(root)
    main.pack(side="top", fill="both", expand=True)
    root.wm_geometry("400x300")
    root.wm_title("Flood Warning System with Weather Integration")
    root.mainloop()