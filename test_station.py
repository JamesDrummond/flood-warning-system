"""Unit test for the station module"""

import pytest
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_typical_range_consistent():
    test_station1 = MonitoringStation("test-s-id-1",
                                      "test-m-id-1",
                                      "station-1",
                                      (1, 1),
                                      None,
                                      "river-1",
                                      "town-1")
    
    test_station2 = MonitoringStation("test-s-id-2",
                                      "test-m-id-2",
                                      "station-2",
                                      (2, 2),
                                      (1, 0),
                                      "river-2",
                                      "town-2")
    
    test_station3 = MonitoringStation("test-s-id-3",
                                      "test-m-id-3",
                                      "station-3",
                                      (3, 3),
                                      (0, 1),
                                      "river-3",
                                      "town-3")
    
    
    assert test_station1.typical_range_consistent() == False
    assert test_station2.typical_range_consistent() == False
    assert test_station3.typical_range_consistent() == True

def test_inconsistent_typical_range_stations():
    test_station1 = MonitoringStation("test-s-id-1",
                                      "test-m-id-1",
                                      "station-1",
                                      (1, 1),
                                      None,
                                      "river-1",
                                      "town-1")
    
    test_station2 = MonitoringStation("test-s-id-2",
                                      "test-m-id-2",
                                      "station-2",
                                      (2, 2),
                                      (1, 0),
                                      "river-2",
                                      "town-2")
    
    test_station3 = MonitoringStation("test-s-id-3",
                                      "test-m-id-3",
                                      "station-3",
                                      (3, 3),
                                      (0, 1),
                                      "river-3",
                                      "town-3")
    list0 = [test_station1, test_station2, test_station3]
    list1 = inconsistent_typical_range_stations(list0)
    assert list1 == [test_station1.name, test_station2.name]
    