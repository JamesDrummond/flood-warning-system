import pickle
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from floodsystem.weathersystem import weather_table, at_risk, flood_risk
import requests
import floodsystem as fs
gmail_user = 'flood.notifications@gmail.com'  
gmail_password = 'part1acomp'

def send(rad, level, refresh):
    if internet_on() and refresh == 'y':
        fs.datafetcher.fetch_station_data(False)
        fs.datafetcher_weather.fetch_weather_data(False)
    mail_list = load_data()
    recipients = at_risk(mail_list, rad, level)
    if len(recipients) > 0:
        email_flood_warning(recipients)
    else:
        pass

def email_flood_warning(recipients):
    
    for recipient in recipients:
        
        msg = MIMEMultipart()
        msg['From'] = gmail_user
        msg['To'] = recipient.email
        msg['Subject'] = "Flood Warning for your local area"
        body = build_email(recipient)
        msg.attach(MIMEText(body, 'plain'))
        
        try:  
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(gmail_user, gmail_password)
            text = msg.as_string()
            server.sendmail(gmail_user, recipient.email, text)
            server.quit()
        
        except:  
            pass

def build_email(recipient):
    locations = str()
    for item in flood_risk(recipient.postcode):
        if item[2] > 1:
            locations += "\n" + item[0]+ " on the " + item[1] + " is showing " + item[3] + " risk. " + "\n"
    name = str(recipient.name)
    body = "Dear " + name + "\n\n" + "The following stations have recorded unusually high water level readings on their respective rivers: \n" + locations + "\n\n" + weather_table(recipient.postcode) 
    return body
    
def load_data():
    try:
        x = pickle.load(open('mailing_list.txt', 'rb'))
    except:
        x = []
    return x
    
def internet_on():
    try:
        requests.get("http://www.google.com")
        return True
    except requests.ConnectionError:
        return False