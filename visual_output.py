import tkinter as tk
import sys
from task_menu import menu

class ExampleApp(tk.Tk):
    def __init__(self, x, y):
        tk.Tk.__init__(self)
        toolbar = tk.Frame(self)
        toolbar.pack(side="bottom", fill="x")
        self.text = tk.Text(self, wrap="word")
        self.text.pack(side="top", fill="both", expand=True)
        self.text.tag_configure("stderr", foreground="#b22222")
        sys.stdout = TextRedirector(self.text, "stdout")
        b1 = tk.Button(self, text="Close", command=self.destroy)
        b1.pack(in_=toolbar, side="right")
        menu(x, y)

class TextRedirector(object):
    def __init__(self, widget, tag="stdout"):
        self.widget = widget
        self.tag = tag

    def write(self, str):
        self.widget.configure(state="normal")
        self.widget.insert("end", str, (self.tag,))
        self.widget.configure(state="disabled")

def run(x, y = ''):
    app = ExampleApp(x, y)
    app.wm_title(x)
    app.wm_geometry("850x400")
    app.mainloop()