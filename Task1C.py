from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()
    # Set location to use as centre
    location = (52.2053, 0.1218)
    # Radius for locus
    radius = 10
    # Retrieve a list of stations within radius
    locus= stations_within_radius(stations, location, radius)
    # Create a list of station names
    names = []
    for i in locus:
        names.append(i.name)
    # Sort the list alphabetically 
    names.sort()
    
    print("\nThe stations within {}km of Cambridge are:{}".format(radius,names))
    
    
if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***\n")

    # Run Task1C
    run()