# Flood Monitoring and Warning System Application with Weather integration

## Introduction:
We have developed a single executable file that combines the flood warning system features required by the 1A Lent Term Computing exercise with some additional features of our own.
This document details the additional features developed for this application and how to use them.

## Summary:
The most significant additional features and functionality:

* Converting the python directory into a single .exe file.
* Adding a user interface, allowing users to find the flood risk in their local area.
* Downloading Weather Forecast Data and incorporating this into the flood risk analysis.
* Collection of ‘broken’ water level monitoring stations to identify where flood risk is unknown.
* An e-mail service including basic mail-list functions to update subscribers on their current local flood risk.

## User Instructions:
### Starting the app:
Run the executable file (weather_app.exe) located in /dist
### Using the application:
Once loaded the Project overview page should be visible. Navigate to different tabs using the toolbar at the top of the window.
Run the demonstration tasks by clicking on their respective buttons on the the 2 Milestone pages.

##### Added Features:
Enter a location in the search bar - can be a postcode, location name or coordinates. (Be careful of ambiguity)
Using the buttons you can view:

* The flood risk for that local area, using both river and weather data
* The weather for the location presented in a table
* Heatmap of the surrounding area showing rivers with high water level (over 0.8 relative level)
* Raw weather data for the nearest weather station
* River monitoring stations nearby that are showing errors in their data and so are not used for flood risk calculations

The buttons on the bottom left are for the whole country:

* "Heatmap" shows all river stations which are showing high relative water level.
* "Show Errors" prints a list of all river stations which are showing errors.

The buttons on the bottom right are controls for the application:

* "Refresh" button lets you refresh the data stored in cache. This takes a few minutes, but only if you have internet connection otherwise you just have the choice to delete cache.
* "Admin" will open the admin section of the program. This is password protected - Username:Admin and Password:123

##### Admin Section:
* Add to mailing list - enter details and press add (If details are invalid the entry will not be added)
* Search mailing list - enter part or all of someones name/postcode and click search (entering nothing will retrive the whole mailing list)
* Delete entry - enter full email address to remove the user

(Please note: the searches and removal are both case sensitive)

###### Start email scheduler:
* Enter the frequency in seconds - has to be greater than 300 to ensure data is downloaded in time (only in seconds for testing - would be set to hours or a specific time each day for real product)
* Set radius for flood warning
* Enter warning level - from 0-10 -- Levels shown below
* Refresh data - weather or not you want to refresh the data before each email

Start scheduler - scheduler will end when the application is closed

## Detailed Explanation of Features:
**User Interface**

A Graphical User Interface was made, from which all task could be run. The screenshots below show the different windows.

![fig1.PNG](https://bitbucket.org/repo/LqgL67/images/732985947-fig1.PNG)  
Figure 1 - Project Overview
 
![fig2.PNG](https://bitbucket.org/repo/LqgL67/images/1605342230-fig2.PNG)  
Figure 2 - Milestone 1 tab
 
![fig3.PNG](https://bitbucket.org/repo/LqgL67/images/2819799540-fig3.PNG)  
Figure 3 - Milestone 2 tab
 
![fig4.PNG](https://bitbucket.org/repo/LqgL67/images/617051257-fig4.PNG)  
Figure 4 - Additional Features tab

The Additional features allow a user to search their location and find the flood risk in their local area. Users can also access weather forecasts and a heat map showing their local water-level monitoring stations.

**Weather Forecast Data Analysis**

Additional Python modules were developed that collected weather data from over 17,000 stations in the UK – using a similar method to the collection of water level data. The MET office provides forecast at all these stations for many weather based parameters – the modules streamline the download process by only collecting the location (in latitude and longitude), chance of precipitation and type of weather at each station.
The type of weather was averaged over five days and scored based on estimates of its impact on the river levels.

![Table 1](https://bitbucket.org/repo/LqgL67/images/2633197519-tab1.PNG)



Immediate Risk due to weather was then found by multiplying the chance of rain with the Weather Severity Score. Flood risk, at each location, was found by summing the Water Level Risk with the Weather Risk indicated by the nearest weather station to it. See the Appendix for risk tables.

**Heatmap**

The heat map function was developed using gmplot – it shows all ‘at-risk’ stations (where the level is above 80% relative level)
 
![fig5.PNG](https://bitbucket.org/repo/LqgL67/images/3233865393-fig5.PNG)  
Figure 5 - Monitoring station heat map of UK

**Data-Handling**

A function was created that collected the names of all monitoring stations with errors in their water level data – important for identifying locations where the error is unknown and could therefore be high.
Functions were also built in to the data collection that refreshed the data every time it was collected.

**Mailing system**

A password protected mailing system was created. Once logged in an admin can:

* Add recipients to the list
* Search the list
* Remove recipients from the list
* Start a scheduler to send out regular emails to everyone in the list with details of their local flood risk.
  
![fig6.PNG](https://bitbucket.org/repo/LqgL67/images/928083930-fig6.PNG)  
Figure 6 - Admin log-in screen
 
![fig7.PNG](https://bitbucket.org/repo/LqgL67/images/201635965-fig7.PNG)  
Figure 7 - Add recipient screen
 
![fig8.PNG](https://bitbucket.org/repo/LqgL67/images/3721201506-fig8.PNG)  
Figure 8 - Scheduler screen
 
![fig9.PNG](https://bitbucket.org/repo/LqgL67/images/28131237-fig9.PNG)  
Figure 9 - example email for resident in Fairford

## Appendix:

**Risk Tables**

For the risk tables: 
Red = Severe
Yellow = High
Green  = Moderate
Clear = Low

![fig10.PNG](https://bitbucket.org/repo/LqgL67/images/509874694-fig10.PNG)  
Figure 10 - Water Level Risk Analysis

![fig11.PNG](https://bitbucket.org/repo/LqgL67/images/250285182-fig11.PNG)  
Figure 11 - Weather Risk Analysis

![fig12.PNG](https://bitbucket.org/repo/LqgL67/images/2888237996-fig12.PNG)  
Figure 10 - Flood Risk Analysis