from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    # Set location to use as centre
    location = (52.2053, 0.1218)
    # Retrieve list of tuples of stations and their distances from location
    distances = stations_by_distance(stations,location)
    # Create a list of tuples containing Station names, towns and distances from location
    names =[]
    for i in distances:
        A = (i[0].name, i[0].town, i[1])
        names.append(A)
    # Print the 10 closest station tuples then 10 furthest station tuples
    print ("\nThe closest stations to Cambridge are:",names[:10])
    print ("\nThe furthest stations from Cambridge are:",names[-10:])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")

    # Run Task1B
    run()
