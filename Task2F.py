import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels_with_fit
from floodsystem.flood import stations_highest_rel_level
from floodsystem.analysis import cleanlist

def run():

    # Build list of stations
    all_stations = build_station_list()
    update_water_levels(all_stations)
    stations, errors = cleanlist(all_stations)
    highest_stations = stations_highest_rel_level(stations, 5)
    dt = 2

    for item in highest_stations:
        station = item[0]
        print(station.name)
        # Fetch data over past 10 days
        dates, levels = fetch_measure_levels(station.measure_id,
                                             dt=datetime.timedelta(days=dt))
        # Plot level history and best fit 4th order polynomial 
        try:
            plot_water_levels_with_fit(station, dates, levels, 4)
        except:
            print(dates, '\n\n', levels)
    print("Stations that were excluded for not having accurate data:\n", errors)
if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")

    run()
    