import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import grad, cleanlist
from floodsystem.utils import sorted_by_key
import numpy as np

def run():
    """Requirements for Task 2G"""

    # Build list of stations
    all_stations = build_station_list()
    # Update the water levels
    update_water_levels(all_stations)
    # Clear erronous stations and add them to separate list
    stations, errors = cleanlist(all_stations)
    # Time period for historical datat in days (2)
    dt=2
    station_scores = []
    counter = 1
    # Cut-off, ignoring stations with relative water levels less than (0.8)
    cutoff = 0.8
    
    for station in stations:
        # Fetch data over past 10 days
        print(counter, "out of", len(stations))
        
        if station.relative_water_level() > cutoff:
            try:
                dates, levels = fetch_measure_levels(station.measure_id,
                                                     dt=datetime.timedelta(days=dt))
                gradient = (np.arctan(grad(dates,levels,4))*2)/np.pi
                # Combine water level and gradient of level with time to get a score between 0 and 2
                score = (1+gradient) * station.relative_water_level()
                # Analyse score and assign a flood risk
                if score < 0.8 :
                    risk = "Low"
                elif score < 1.5:
                    risk = "Moderate"
                elif score < 3:
                    risk = "High"
                else:
                    risk = "Severe"
                score = str(round(score, 2))
                station_scores.append((station,score,risk))
            except:
                # Add erronous stations to the error list
                errors.append(station.name)
            
        counter += 1
    # Sort list by score
    list_sorted = sorted_by_key(station_scores, 1, True)
    
    ####Print####
    print ("The towns at greatest risk of flooding are:")
    a=0
    for item in list_sorted:
        if item[2] == "Severe" or a < 10 :
            a += 1
            print ("{} where there is a {} risk of flooding \n   Score = {}".format (item[0].town, item[2], item[1]))
        else:
            pass
        
    
    
    if len(errors) != 0:
        print (" There were errors with the following monitoring stations: \n", errors)

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")

    # Run Task2G
    run()