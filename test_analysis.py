import pytest
import dateutil.parser
from floodsystem.analysis import polyfit, grad, cleanlist
import numpy as np
from floodsystem.station import MonitoringStation

dates_string = ['0001-01-01 00:00:00+00:00',
'0001-01-02 00:00:00+00:00',
'0001-01-03 00:00:00+00:00',
'0001-01-04 00:00:00+00:00',
'0001-01-05 00:00:00+00:00',
'0001-01-06 00:00:00+00:00',
'0001-01-07 00:00:00+00:00',
'0001-01-08 00:00:00+00:00',
'0001-01-09 00:00:00+00:00',
'0001-01-10 00:00:00+00:00']

dates = []

for item in dates_string:
    d = dateutil.parser.parse(item)
    dates.append(d)
    
    
levels = [1,1,1,1,1,1,1,1,1,1]

def test_polyfit():
    test = polyfit(dates, levels, 1)
    poly =  np.poly1d([ -3.60156447e-20,   1.00000000e+00])
    assert str(test) == str((poly, 1.0))

def test_grad():
    test = grad(dates, levels, 1)
    assert test == -3.6015644721394473e-20

def test_cleanlist():
    test_station1 = MonitoringStation("test-s-id-1",
                                      "test-m-id-1",
                                      "station-1",
                                      (1, 1),
                                      None,
                                      "river-1",
                                      "town-1")
    
    test_station2 = MonitoringStation("test-s-id-2",
                                      "test-m-id-2",
                                      "station-2",
                                      (2, 2),
                                      (1, 0),
                                      "river-2",
                                      "town-2")
    
    test_station3 = MonitoringStation("test-s-id-3",
                                      "test-m-id-3",
                                      "station-3",
                                      (3, 3),
                                      (0, 1),
                                      "river-3",
                                      "town-3")
    test_station4 = MonitoringStation("test-s-id-4",
                                      "test-m-id-4",
                                      "station-4",
                                      (4, 4),
                                      (0, 1),
                                      "river-4",
                                      "town-4")
    test_station5 = MonitoringStation("test-s-id-5",
                                      "test-m-id-5",
                                      "station-5",
                                      (5, 5),
                                      (0, 1),
                                      "river-5",
                                      "town-5")
    test_station1.latest_level = 1
    test_station2.latest_level = 1
    test_station3.latest_level = None
    test_station4.latest_level = 20
    test_station5.latest_level = 0.5
    
    stations = [test_station1, test_station2, test_station3, test_station4, test_station5]
    test = cleanlist(stations)
    assert test == ([test_station5],['station-1', 'station-2', 'station-3', 'station-4'])