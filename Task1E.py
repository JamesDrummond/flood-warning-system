

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def run():
    """Requirements for Task 1E"""

    # Build list of stations
    stations = build_station_list()
    # Retrieve a list of tuples (rivers, numbers of stations) sorted by number of stations, showing the top 9
    most_stations = rivers_by_station_number(stations, 9)
    print("The rivers with the most monitoring stations are:",most_stations)
    
    

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***\n")

    # Run Task1E
    run()
