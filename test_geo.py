# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 15:52:36 2017

@author: Sam
"""

"""Unit test for the stationdata module"""

import pytest
from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number

MonitoringStation1 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/1029TH" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/1029TH-level-stage-i-15_min-mASD" ,
                                       "Bourton Dickler" ,
                                       (51.874767, -1.740083) ,
                                       (0.068, 0.42) ,
                                       "Dickler" ,
                                       "Little Rissington")
MonitoringStation2 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/E2043" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/E2043-level-stage-i-15_min-mASD" ,
                                       "Surfleet Sluice" ,
                                       (52.845991, -0.100848) ,
                                       (0.15, 0.895) ,
                                       "River Glen" ,
                                       "Surfleet Seas End")
MonitoringStation3 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/52119" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/52119-level-stage-i-15_min-mASD" ,
                                       "Gaw Bridge" , 
                                       (50.976043, -2.793549) ,
                                       (0.231, 0.971) ,
                                       "River Parrett" ,
                                       "Kingsbury Episcopi")

MonitoringStation4 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/52116" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/52116-level-stage-i-15_min-m" ,
                                       "Chiselborough" , 
                                       (50.976043, -2.793549) ,
                                       (0.231, 0.971) ,
                                       "River Parrett" ,
                                       "Chiselborough")

stations= [MonitoringStation1,MonitoringStation2,MonitoringStation3]
stations1 = [MonitoringStation1,MonitoringStation2,MonitoringStation3,MonitoringStation4]


def test_stations_by_distance():
    test = stations_by_distance(stations, (52.2053, 0.1218))
    assert test == [(MonitoringStation2, 72.81640383176862), (MonitoringStation1, 132.54084758653366), (MonitoringStation3, 243.37423100276914)]

def test_stations_within_radius():
    test = stations_within_radius(stations, (52.2053, 0.1218), 150)
    assert test == [MonitoringStation1,MonitoringStation2]

def test_rivers_with_station():
    test = rivers_with_station(stations1)
    assert test == {'Dickler', 'River Glen', 'River Parrett'}
    
def test_stations_by_river():
    test = stations_by_river(stations1)
    assert test ==  {'Dickler': ['Bourton Dickler'], 'River Glen': ['Surfleet Sluice'], 'River Parrett': ['Chiselborough', 'Gaw Bridge']}
    
def test_rivers_by_station_number():
    test = rivers_by_station_number(stations1, 2)
    assert test == [('River Parrett', 2), ('River Glen', 1), ('Dickler', 1)] or [('River Parrett', 2), ('Dickler', 1), ('River Glen', 1)]

