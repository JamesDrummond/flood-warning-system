import pytest
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level

MonitoringStation1 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/1029TH" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/1029TH-level-stage-i-15_min-mASD" ,
                                       "Bourton Dickler" ,
                                       (51.874767, -1.740083) ,
                                       (0.068, 0.42) ,
                                       "Dickler" ,
                                       "Little Rissington")
MonitoringStation2 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/E2043" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/E2043-level-stage-i-15_min-mASD" ,
                                       "Surfleet Sluice" ,
                                       (52.845991, -0.100848) ,
                                       (0.15, 0.895) ,
                                       "River Glen" ,
                                       "Surfleet Seas End")
MonitoringStation3 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/52119" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/52119-level-stage-i-15_min-mASD" ,
                                       "Gaw Bridge" , 
                                       (50.976043, -2.793549) ,
                                       (0.231, 0.971) ,
                                       "River Parrett" ,
                                       "Kingsbury Episcopi")

MonitoringStation4 = MonitoringStation("http://environment.data.gov.uk/flood-monitoring/id/stations/52116" ,
                                       "http://environment.data.gov.uk/flood-monitoring/id/measures/52116-level-stage-i-15_min-m" ,
                                       "Chiselborough" , 
                                       (50.976043, -2.793549) ,
                                       (0, 1) ,
                                       "River Parrett" ,
                                       "Chiselborough")

MonitoringStation1.latest_level = 0
MonitoringStation2.latest_level = 0.2
MonitoringStation3.latest_level = 1
MonitoringStation4.latest_level = 2

stations = [MonitoringStation1,MonitoringStation2,MonitoringStation3,MonitoringStation4]

def test_stations_level_over_threshold():
    test = stations_level_over_threshold(stations, 1.5)
    assert test == [(MonitoringStation4, 2.0)]

def test_stations_highest_rel_level():
    test = stations_highest_rel_level(stations, 2)
    assert test == [(MonitoringStation4, 2.0),(MonitoringStation3, 1.0391891891891891) ]