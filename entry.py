class MailRecipient:
    """This class represents a river level monitoring station"""

    def __init__(self, name, email, postcode, coord):

        self.name = name
        self.email = email
        self.postcode = postcode
        self.coord = coord

    def __repr__(self):
        d = "{}\n".format(self.name)
        d += "{}\n".format(self.email)
        d += "{}\n".format(self.postcode)
        d += "{}\n".format(self.coord)
        return d