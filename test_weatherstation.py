"""Unit test for the weatherstation module"""

import pytest
from floodsystem.weatherstation import WeatherStation
from dateutil import parser

def test_create_monitoring_station():

    # Create a weather station
    station_id = "test-s-id"
    name = "some station"
    coord = (1, 1)
    weather = [0, 0, 0, 0, 0]
    rain = [1, 1, 1, 1, 1]
    temp = [2, 2, 2, 2, 2]
    time = '2017-02-17 14:00:00+00:00'
    s = WeatherStation(station_id, name, coord, weather, rain, temp, time)

    assert s.station_id == station_id
    assert s.name == name
    assert s.coord == coord
    assert s.weather == weather
    assert s.rain == rain
    assert s.temp == temp
    assert s.time == parser.parse(time)