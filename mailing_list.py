import geocoder
import pickle
from entry import MailRecipient
from validate_email import validate_email

def add(name, email, postcode):
    if name == '' or email == '' or postcode == '':
        pass
    else:
        g = geocoder.google(postcode)
        is_valid = validate_email(email)
        if str(g) != '<[ZERO_RESULTS] Google - Geocode>' and is_valid:
            coords = g.latlng
            recipient = MailRecipient(name, email, postcode, coords)
            x = load_data()
            x.append(recipient)
            save_data(x)
        else:
            pass
    
def remove(email):
    people = load_data()
    for person in people:
        if email == person.email:
            people.remove(person)
    save_data(people)
    
def retrieve(search):
    people = load_data()
    results = []
    for person in people:
        if search in person.name:
            results.append(person)
        elif search in person.postcode:
            results.append(person)
    if len(results) == 0:
        results = "No search results found"
    return results
    
def load_data():
    try:
        x = pickle.load(open('mailing_list.txt', 'rb'))
    except:
        x = []
    return x
    
def save_data(data):
    pickle.dump(data, open('mailing_list.txt', 'wb'))
    