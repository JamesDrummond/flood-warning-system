

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river

def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()
    # Retrieve set of all rivers
    rivers = rivers_with_station(stations) 
    # Convert to list
    rivers_list = list(rivers)
    # Sort list
    rivers_list.sort()
    print("First 10 Rivers in alphabetical order", rivers_list[:10],"\n")
    # Retrieve dictionary of Rivers to Stations 
    river_stations = stations_by_river(stations)
    print("Stations on the River Aire:",river_stations["River Aire"],"\n")
    print("Stations on the River Cam:",river_stations["River Cam"],"\n")
    print("Stations on the River Thames:",river_stations["Thames"],"\n")

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***\n")

    # Run Task1D
    run()
